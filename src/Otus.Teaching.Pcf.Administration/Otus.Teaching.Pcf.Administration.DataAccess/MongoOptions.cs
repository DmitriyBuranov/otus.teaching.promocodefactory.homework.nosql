﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public class MongoOptions : IMongoOptions
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }
}
